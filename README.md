**FXT ( Flash Extensible Technique )**

@author Ken Murray

@date 4/25/19

*Creates a dynamic UI similar to how EXTJS does or used to extend functionality EXTJS lacks*

Inspired by ExtJS and Flash/ActionScript.
Simplifies common JS calls to DOM objects in a familiar format.
Current item depth is 10 levels MAX.

---

**FXT Example**

 *Bootstrap 4 - examples/BootStrap4Modal.html* 
 
 Converted to FXT from original html source - https://www.w3schools.com/bootstrap4/tryit.asp?filename=trybs_modal&stacked=h
 
 